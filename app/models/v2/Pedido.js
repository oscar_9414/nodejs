const mongoose = require('mongoose')
const Schema = mongoose.Schema
const User = require('./User.js')

const pedidoSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'User'
  },
  dateCreation: {
    default: Date.now,
    type: Date
  },
  productos: [
    {
      product: {
        type: Schema.Types.ObjectId,
        ref: 'Product'
      },
      cantidad: {
        type: Number
      },
      precio: {
        type: Number
      }
    }
  ]
})

const Pedido = mongoose.model('Pedido', pedidoSchema)

module.exports = Pedido
