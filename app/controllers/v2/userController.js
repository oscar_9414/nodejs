// npm i -S jwt-simple
// const mongoose = require('mongoose')
const User = require('../../models/v2/User')
const servicejwt = require('../../service/servicejwt')

const index = (req, res) => {
  User.find()
    .populate('user_id')
    .exec((err, user) => {
      if (err) {
        return res.status(500).json({
          message: 'Error al obtener los usuarios'
        })
      }
      return res.json(user)
    })
}
const register = (req, res) => {
  const user = new User({
    email: req.body.email,
    name: req.body.name,
    password: req.body.password
  })
  user.save(err => {
    if (err) res.status(500).send({ message: `Error al crear usuario: ${err}` })
    // servicejwt nos va a crear un token
    return res.status(200).send({ token: servicejwt.createToken(user) })
  })
}

module.exports = {
  index,
  register
}
