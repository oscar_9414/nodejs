const express = require('express')
const router = express.Router() // son para crear el enrutador
const userController = require('../../controllers/v2/userController')

// registro de un nuevo usuario
router.get('/', (req, res) => {
  userController.index(req, res)
})
router.post('/', userController.register) // seria algo así como registrar un nuevo usuario

module.exports = router
