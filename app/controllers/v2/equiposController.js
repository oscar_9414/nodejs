const Equipo = require('../../models/v2/Equipo')
const { ObjectId } = require('mongodb')
const index = (req, res) => {
  Equipo.find((err, equipos) => {
    if (err) {
      return res.status(500).json({
        message: 'Error al obtener el equipos'
      })
    }
    return res.json(equipos)
  })
}

const create = (req, res) => {
  const equipo = new Equipo(req.body)
  equipo.save((err, equipo) => {
    if (err) {
      return res.status(400).json({
        message: 'Error al guardar el equipo',
        error: err
      })
    }
    return res.status(201).json(equipo)
  })
}

const update = (req, res) => {
  const id = req.params.id
  Equipo.findOne({ _id: id }, (err, equipo) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (err) {
      return res.status(500).json({
        message: 'Se ha producido un error al guardar el Equipo',
        error: err
      })
    }
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (!equipo) {
      return res.status(404).json({
        message: 'No hemos encontrado el Equipo'
      })
    }

    Object.assign(equipo, req.body)

    equipo.save((err, equipo) => {
      if (err) {
        return res.status(500).json({
          message: 'Error al guardar el Equipo'
        })
      }
      if (!equipo) {
        return res.status(404).json({
          message: 'No hemos encontrado el Equipo'
        })
      }
      return res.json(equipo)
    })
  })
}

const show = (req, res) => {
  const id = req.params.id
  console.log('hola')
  Equipo.findOne({ _id: id }, (err, equipo) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (err) {
      return res.status(500).json({
        message: 'Se ha producido un error al obtener el equipo'
      })
    }
    if (!equipo) {
      return res.status(404).json({
        message: 'No tenemos este equipo'
      })
    }
    return res.json(equipo)
  })
}

const remove = (req, res) => {
  const id = req.params.id

  Equipo.findByIdAndDelete({ _id: id }, (err, equipo) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (err) {
      return res.json(500, {
        message: 'No hemos encontrado el equipo'
      })
    }
    if (!equipo) {
      return res.status(404).json({
        message: 'No hemos encontrado el equipo'
      })
    }
    return res.json(equipo)
  })
}

module.exports = {
  index,
  create,
  update,
  show,
  remove
}
