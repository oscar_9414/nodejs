const mongoose = require('mongoose')
const Schema = mongoose.Schema

const equipoSchema = new Schema({
  key: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  code: {
    type: String,
    required: true
  }
})

const Equipo = mongoose.model('Equipo', equipoSchema)

module.exports = Equipo
