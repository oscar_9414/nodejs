const Pedido = require('../../models/v2/Pedido')
const User = require('../../models/v2/User')

const { ObjectId } = require('mongodb')

const index = (req, res) => {
  console.log('Lista de os pedidos')
  Pedido.find()
    .populate('user')
    .populate('productos.product')
    .exec((err, pedido) => {
      if (err) {
        return res.status(500).json({
          message: 'Error al obtener los pedidos'
        })
      }
      return res.json(pedido)
    })
}

const show = (req, res) => {
  console.log('ostrando el pedido')
  const id = req.params.id
  Pedido.findById(id)
    .populate('user')
    .exec(function (err, pedido) {
      if (err) {
        return res.status(500).json({
          message: 'Error al mostrar el pedido'
        })
      }
      if (pedido == null) {
        pedido = 'Lo sentimos pero no hemos encontrado el producto'
      }
      console.log(pedido)
      return res.json(pedido)
    })
}

const create = (req, res) => {
  console.log('creando el pedido')
  const pedido = new Pedido(req.body)
  pedido.save(
    {
      user: pedido.user_id
    },
    function (err, pedido) {
      if (err) {
        return res.json({ error: err.errors.name })
      }
      return res.json(pedido)
    }
  )
}

const update = (req, res) => {
  const id = req.params.id
  Pedido.findOne({ _id: id }, (err, pedido) => {
    console.log(pedido)
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (err) {
      return res.status(500).json({
        message: 'Se ha producido un error al guardar el pedido',
        error: err
      })
    }
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (!pedido) {
      return res.status(404).json({
        message: 'No hemos encontrado el pedido'
      })
    }

    Object.assign(pedido, req.body)

    pedido.save((err, pedido) => {
      if (err) {
        return res.status(500).json({
          message: 'Error al guardar el pedido'
        })
      }
      if (!pedido) {
        return res.status(404).json({
          message: 'No hemos encontrado el pedido'
        })
      }
      return res.json(pedido)
    })
  })
}

const remove = (req, res) => {
  console.log('borrando el pedido')
  const id = req.params.id
  Pedido.findOneAndDelete({ _id: id }, (err, pedido) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (err) {
      return res.json(500, {
        message: 'No hemos encontrado el pedido'
      })
    }
    if (!pedido) {
      return res.status(404).json({
        message: 'No hemos encontrado el pedido'
      })
    }
    return res.json(pedido)
  })
}

module.exports = {
  index,
  show,
  create,
  update,
  remove
}
