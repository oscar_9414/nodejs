const mysql = require('mysql2')
const connection = mysql.createConnection({
  user: 'root',
  password: 'root',
  host: 'localhost',
  database: 'cervezas'
})

const index = (req, res) => {
  console.log('Lista de cerveza')
  const sql = 'SELECT * FROM cervezas'
  connection.query(sql, (err, result, fields) => {
    if (err) {
      res.status(500).json('error')
    } else {
      res.json(result)
    }
  })
  // res.json({ mensaje: ` Lista de cervezas desde controller` })
}

const show = (req, res) => {
  console.log('Lista de cerveza')
  const id = req.params.id
  const sql = 'SELECT * FROM cervezas WHERE id = ?'
  connection.query(sql, [id], (err, result, fields) => {
    if (err) {
      res.status(500).json('error')
    } else {
      res.json(result)
    }
  })
  // res.json({ mensaje: ` mostrando detalle de la cerveza ${id} desde controller` })
}

const store = (req, res) => {
  name = req.body.name
  description = req.body.description
  alcohol = req.body.alcohol
  container = req.body.container
  price = req.body.price
  console.log(name, container, description, price, alcohol)
  const sql =
    'INSERT INTO cervezas ( name, description,alcohol,container,price)' +
    ' VALUES (?,?,?,?,?)'

  connection.query(
    sql,
    [name, description, alcohol, container, price],
    (err, result, fields) => {
      if (err) {
        res.status(500).json('error')
      } else {
        res.json(result)
      }
    }
  )

  // res.json({ mensaje: ` guardando ${req.body.nombre} desde controller ` })
}

const update = (req, res) => {
  id = req.params.id
  name = req.body.name
  console.log(name)
  const sql = 'UPDATE cervezas SET name = ? WHERE id = ?'
  connection.query(sql, [req.body.name, id], (err, result, fields) => {
    if (err) {
      res.status(500).json('error')
    } else {
      res.json(result)
    }
  })
  // console.log("Lista de cerveza")
  // const id = req.params.id
  // res.json({ mensaje: ` Cerveza ${id} actualizada desde controller` })
}

const destroy = (req, res) => {
  console.log('Lista de cerveza')
  const id = req.params.id
  res.json({ mensaje: ` cerveza borrada desde controller` })
}
module.exports = {
  index,
  show,
  store,
  destroy,
  update
}
