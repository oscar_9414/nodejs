const express = require('express')
// para establecer las distintas rutas, necesitamos instanciar el express router
const router = express.Router()
equipoController = require('../../controllers/v2/equiposController')
// const auth = require('../../middlewares/auth')

// para proteger todas las rutass:
// router.use(auth)

// //para proteger una sóla ruta
// router.get('/', auth, (req, res) => {
//   equipoController.index(req, res)
// })

// //o así
// router.get('/', auth, equipoController.index})

// router.use(auth.auth) // usando el modulo de middlewares/auth

router.get('/', (req, res) => {
  equipoController.index(req, res)
})

router.post('/', (req, res) => {
  equipoController.create(req, res)
})

router.get('/:id', (req, res) => {
  equipoController.show(req, res)
})

router.delete('/:id', (req, res) => {
  equipoController.remove(req, res)
})

router.put('/:id', (req, res) => {
  equipoController.update(req, res)
})

// router.get('/search', (req, res) => {
//   console.log('rutas de products')
//   equipoController.search(req, res)
// })
// router.get('/:id', (req, res) => {
//   console.log('rutas de products')
//   equipoController.show(req, res)
// })

module.exports = router
