const express = require('express') // llamamos a Express
// para establecer las distintas rutas, necesitamos instanciar el express router
const router = express.Router()

const routerCervezas = require('./routes/v2/cervezas.js')
const routerProducts = require('./routes/v2/productos.js')
const routerUsers = require('./routes/v2/users.js')
const routerEquipos = require('./routes/v2/equipos.js')
const routerPedidos = require('./routes/v2/pedidos.js')

// PRUEBA DE AMBAR

const Cerveza = require('./models/v2/Cerveza')

// establecemos nuestra primera ruta, mediante get.
router.get('/', (req, res) => {
  res.json({ mensaje: '¡Bienvenido a nuestra API con MongoDB!' })
})

// router.get('/ambar', (req, res) => {
//   const miCerveza = new Cerveza({ nombre: 'Ambar' })
//   miCerveza.save((err, miCerveza) => {
//     if (err) return console.error(err)
//     console.log(`Guardada en bbdd ${miCerveza.nombre}`)
//   })
// })
router.use('/cervezas', routerCervezas)
router.use('/products', routerProducts)
router.use('/users', routerUsers)
router.use('/equipos', routerEquipos)
router.use('/pedidos', routerPedidos)

module.exports = router // EXPORTAMOS EL ROUTER
