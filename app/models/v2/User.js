const mongoose = require('mongoose')
const Schema = mongoose.Schema
// cifrado de passwords.
const bcrypt = require('bcrypt-nodejs')

// aquí falta lo importante
const UserSchema = new Schema({
  email: {
    type: String,
    unique: true,
    lowercase: true // lo guardará en minúsculas
  },
  name: String,
  password: {
    type: String,
    select: false // cuando se devuelvan los datos no se devuelve la contraseña
  },
  login: {
    type: Date,
    default: Date.now()
  },
  lastLogin: Date
})

UserSchema.pre('save', function (next) {
  const user = this
  if (!user.isModified('password')) return next()

  bcrypt.genSalt(10, (err, salt) => {
    if (err) return next(err)

    bcrypt.hash(user.password, salt, null, (err, hash) => {
      if (err) return next(err)

      user.password = hash
      next()
    })
  })
})

// exportamos el esquema correspondiente
module.exports = mongoose.model('User', UserSchema)
// un middleware se ejecuta antes op despues del save o create etc...
