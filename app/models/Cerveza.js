const connection = require('../config/dbconnection')
const index = function (callback) {
  const sql = 'SELECT * FROM cervezas'
  connection.query(sql, (err, result, fields) => {
    if (err) {
      callback(500, err)
    } else {
      // console.log(result)
      callback(200, result, fields)
    }
  })
}

const find = function (id, callback) {
  const sql = 'SELECT * FROM cervezas WHERE id=?'
  connection.query(sql, [id], (err, result, fields) => {
    if (err) {
      callback(500, err)
    } else {
      callback(200, result, fields)
    }
  })
}
// fields contenido resultado

const store = function (obj, callback) {
  const sql =
    `INSERT INTO cervezas (name, description, alcohol, container, price)` +
    `VALUES(?,?,?,?,?)`
  connection.query(
    sql,
    [obj.name, obj.description, obj.alcohol, obj.container, obj.price],
    (err, result, fields) => {
      if (err) {
        callback(500, err)
      } else {
        callback(200, result, fields)
      }
    }
  )
}

const update = function (name, container, id, callback) {
  const sql = 'UPDATE cervezas SET name=?, container=? WHERE id=?'

  connection.query(sql, [name, container, id], (err, result) => {
    if (err) {
      callback(500, err)
    } else {
      callback(200, result)
    }
  })
}

const destroy = function (id, callback) {
  const sql = 'DELETE FROM cervezas WHERE id=?'
  connection.query(sql, [id], (err, result, fields) => {
    if (err) {
      callback(500, err)
    } else {
      callback(200, result, fields)
    }
  })
}

module.exports = {
  index,
  find,
  store,
  update,
  destroy
}
