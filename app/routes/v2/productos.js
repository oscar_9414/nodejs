const express = require('express')
// para establecer las distintas rutas, necesitamos instanciar el express router
const router = express.Router()
const productController = require('../../controllers/v2/productController')
const auth = require('../../middlewares/auth')

// para proteger todas las rutass:
// router.use(auth)

// //para proteger una sóla ruta
// router.get('/', auth, (req, res) => {
//   productController.index(req, res)
// })

// //o así
// router.get('/', auth, productController.index})

// router.use(auth.auth) // usando el modulo de middlewares/auth

router.get('/', (req, res) => {
  productController.index(req, res)
})

router.post('/', (req, res) => {
  productController.create(req, res)
})

router.get('/:id', (req, res) => {
  productController.show(req, res)
})

router.delete('/:id', (req, res) => {
  productController.destroy(req, res)
})

router.put('/:id', (req, res) => {
  productController.update(req, res)
})

// router.get('/search', (req, res) => {
//   console.log('rutas de products')
//   productController.search(req, res)
// })
// router.get('/:id', (req, res) => {
//   console.log('rutas de products')
//   productController.show(req, res)
// })

module.exports = router
